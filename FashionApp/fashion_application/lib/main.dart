import 'package:fashion_application/Cart.dart';
import 'package:fashion_application/Checkout.dart';
import 'package:fashion_application/Details.dart';
import 'package:fashion_application/Home.dart';
import 'package:fashion_application/Onboarding.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: Onboarding(),
      // home: Home(),
      // home: Details(),
       home: Cart(),
      // home: Checkout(),
    );
  }
}
